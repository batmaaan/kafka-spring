package org.kafka.spring.kafkaspring;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Employee {
    private Integer id;
    private String name;
}
